#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np



class Grafo:

	def __init__(self,vertices,diagonalSoma = 1):
		self.__inputVerticeIgualDiagonalSoma = int(diagonalSoma)
		if self.__inputVerticeIgualDiagonalSoma < 1:
			self.__inputVerticeIgualDiagonalSoma  = 1
		if self.__inputVerticeIgualDiagonalSoma > 2:
			self.__inputVerticeIgualDiagonalSoma = 2
		self.__vertices = vertices
		self.__matriz = np.zeros((vertices,vertices),dtype=np.int)
		self.__direcionadoCache = False
		self.__direcionado = False
	
	def getGrafoType(self):
		return 'matriz'

	def getDados(self):
		return np.copy(self.__matriz)

	def getTotalVertices(self):
		return self.__vertices

	def __getVerticeInt(self,intVertice,exceptionMsg = 0):
		intVertice = int(intVertice)
		if intVertice > self.__vertices or intVertice < 1:
			if exceptionMsg == 0:
				exceptionMsg = 'Vertice invalido: '+str(intVertice)
			raise Exception(exceptionMsg)
		return intVertice - 1

	def getVerticeTotalAresta(self,verticeDe,verticePara):
		return self.__matriz[verticeDe][verticePara]

	def getArestasVertice(self,verticeDe,verticePara):
		return np.arange(self.getVerticeTotalAresta(verticeDe,verticePara))

	def AdicionaAresta(self,verticeDe,verticePara):
		soma = 1
		if verticeDe == verticePara:
			soma = self.__inputVerticeIgualDiagonalSoma
		verticeDe = self.__getVerticeInt(verticeDe,'Aresta invalida: '+str(verticeDe)+' -> '+str(verticePara))
		verticePara = self.__getVerticeInt(verticePara,'Aresta invalida: '+str(verticeDe+1)+' -> '+str(verticePara))
		self.__matriz[verticeDe][verticePara] += soma
		self.__direcionadoCache = False
		

	def VerificaDirecionado(self):
		if self.__direcionadoCache:
			return self.__direcionado	
		self.__direcionadoCache = True
		for x,linha in enumerate(self.__matriz):
			for y,coluna in enumerate(linha):
				valueLinha = self.getVerticeTotalAresta(x,y)
				valueColuna = self.getVerticeTotalAresta(y,x)
				if (valueLinha != valueColuna) or (x == y and valueLinha % 2 != 0):
					self.__direcionado = True
					return True				
		self.__direcionado = False					
		return False

	def GrauEntrada(self,vertice):
		if not self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x in xrange(self.__vertices):
			grau+=self.getVerticeTotalAresta(x,vertice)
		return int(grau)

	def GrauSaida(self,vertice):
		if not self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x in xrange(self.__vertices):
			grau+=self.getVerticeTotalAresta(vertice,x)
		return int(grau)

	def Grau(self,vertice):
		if self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x in xrange(self.__vertices):
			grau+=self.getVerticeTotalAresta(x,vertice)
		return int(grau)

	def Transposto(self):
		if not self.VerificaDirecionado():
			return False		
		matriz_copy = np.zeros((self.__vertices,self.__vertices),dtype=np.int);
		for i,linha in enumerate(self.__matriz):
			for b,coluna in enumerate(linha):
				 matriz_copy[b][i] = self.getVerticeTotalAresta(i,b)

		self.__matriz = matriz_copy
		return True

	
	@staticmethod
	def Uniao(g1, g2):
		verticesG1 = g1.getTotalVertices()
		verticesG2 = g2.getTotalVertices()
		grafo = Grafo(verticesG1+verticesG2)
		uniaoLinha = 0
		uniaoColuna = 0
		for i in xrange(verticesG1):
			uniaoColuna = 0
			for b in xrange(verticesG1):
				arestas = g1.getArestasVertice(i,b)
				for x in arestas:
					grafo.AdicionaAresta(uniaoLinha+1,uniaoColuna+1)
				uniaoColuna+=1
			uniaoLinha+=1
		uniaoColunaSet = uniaoColuna
		for i in xrange(verticesG2):
			uniaoColuna = uniaoColunaSet
			for b in xrange(verticesG2):
				arestas = g2.getArestasVertice(i,b)
				for x in arestas:
					grafo.AdicionaAresta(uniaoLinha+1,uniaoColuna+1)
				uniaoColuna+=1
			uniaoLinha+=1
		return grafo


	@staticmethod
	def Soma(g1,g2):
		verticesG1 = g1.getTotalVertices()
		verticesG2 = g2.getTotalVertices()
		grafo = Grafo.Uniao(g1,g2)
		for i in xrange(verticesG1):
			for x in xrange(verticesG2):
				grafo.AdicionaAresta(i+1,verticesG1+x+1)

		return grafo


