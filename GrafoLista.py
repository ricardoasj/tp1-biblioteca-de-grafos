#!/usr/bin/python
# -*- coding: utf-8 -*-
import numpy as np



class Grafo:

	def __init__(self,vertices,diagonalSoma = 2):
		self.__inputVerticeIgualDiagonalSoma = int(diagonalSoma)
		if self.__inputVerticeIgualDiagonalSoma < 1:
			self.__inputVerticeIgualDiagonalSoma  = 1
		if self.__inputVerticeIgualDiagonalSoma > 2:
			self.__inputVerticeIgualDiagonalSoma = 2
		self.__vertices = vertices
		self.__lista = []
		for x in xrange(self.__vertices):
			self.__lista.append({})
		self.__direcionadoCache = False
		self.__direcionado = False

	def getGrafoType(self):
		return 'lista'

	def getDados(self):
		return np.copy(self.__lista)

	def getTotalVertices(self):
		return self.__vertices
	
	def __getVerticeInt(self,intVertice,exceptionMsg = 0):
		intVertice = int(intVertice)
		if intVertice > self.__vertices or intVertice < 1:
			if exceptionMsg == 0:
				exceptionMsg = 'Vertice invalido: '+str(intVertice)
			raise Exception(exceptionMsg)
		return int(intVertice - 1)

	def getVerticeTotalAresta(self,verticeDe,verticePara):
		if verticePara in self.__lista[verticeDe]:
			return self.__lista[verticeDe][verticePara]
		return 0
	
	def getArestasVertice(self,verticeDe,verticePara):
		return np.arange(self.getVerticeTotalAresta(verticeDe,verticePara))

	def AdicionaAresta(self,verticeDe,verticePara):
		soma = 1
		if verticeDe == verticePara:
			soma = self.__inputVerticeIgualDiagonalSoma
		verticeDe = self.__getVerticeInt(verticeDe,'Aresta invalida: '+str(verticeDe)+' -> '+str(verticePara))
		verticePara = self.__getVerticeInt(verticePara,'Aresta invalida: '+str(verticeDe+1)+' -> '+str(verticePara))
		if verticePara not in self.__lista[verticeDe]:
			self.__lista[verticeDe][verticePara] = 0
		self.__lista[verticeDe][verticePara] += soma
		self.__direcionadoCache = False

	def VerificaDirecionado(self):
		if self.__direcionadoCache:
			return self.__direcionado	
		self.__direcionadoCache = True
		for vertice,dic in enumerate(self.__lista):
			for adjacencia in self.__lista[vertice]:
				if vertice not in self.__lista[adjacencia] or self.getVerticeTotalAresta(vertice,adjacencia) != self.getVerticeTotalAresta(adjacencia,vertice) or (vertice == adjacencia and  self.getVerticeTotalAresta(vertice,adjacencia) % 2 != 0):
					self.__direcionado = True
					return True
		self.__direcionado = False					
		return False

	def GrauEntrada(self,vertice):
		if not self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x,dic in enumerate(self.__lista):
			if vertice not in self.__lista[x]:
				continue
			grau+=self.getVerticeTotalAresta(x,vertice)
		return int(grau)

	def GrauSaida(self,vertice):
		if not self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x in (self.__lista[vertice]):
			grau+=self.getVerticeTotalAresta(vertice,x)
		return int(grau)	

	def Grau(self,vertice):
		if self.VerificaDirecionado():
			return None
		vertice = self.__getVerticeInt(vertice)
		grau = 0
		for x in (self.__lista[vertice]):
			grau+=self.getVerticeTotalAresta(vertice,x)
		return int(grau)

	def Transposto(self):
		if not self.VerificaDirecionado():
			return False		
		lista = []
		for x in xrange(self.__vertices):
			lista.append({})
		for x,dic in enumerate(self.__lista):
			for adjacencia in self.__lista[x]:
				lista[adjacencia][x] = self.__lista[x][adjacencia]
		self.__lista = lista
		return True
	
	@staticmethod
	def Uniao(g1, g2):
		verticesG1 = g1.getTotalVertices()
		verticesG2 = g2.getTotalVertices()
		grafo = Grafo(verticesG1+verticesG2)
		uniaoLinha = 0
		uniaoColuna = 0
		for i in xrange(verticesG1):
			uniaoColuna = 0
			for b in xrange(verticesG1):
				arestas = g1.getArestasVertice(i,b)
				for x in arestas:
					grafo.AdicionaAresta(uniaoLinha+1,uniaoColuna+1)
				uniaoColuna+=1
			uniaoLinha+=1
		uniaoColunaSet = uniaoColuna
		for i in xrange(verticesG2):
			uniaoColuna = uniaoColunaSet
			for b in xrange(verticesG2):
				arestas = g2.getArestasVertice(i,b)
				for x in arestas:
					grafo.AdicionaAresta(uniaoLinha+1,uniaoColuna+1)
				uniaoColuna+=1
			uniaoLinha+=1
		return grafo


	@staticmethod
	def Soma(g1,g2):
		verticesG1 = g1.getTotalVertices()
		verticesG2 = g2.getTotalVertices()
		grafo = Grafo.Uniao(g1,g2)
		for i in xrange(verticesG1):
			for x in xrange(verticesG2):
				grafo.AdicionaAresta(i+1,verticesG1+x+1)

		return grafo



