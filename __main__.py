#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys
import pprint
import numpy as np
import sys


arquivoEntrada = 'input.txt' # Arquivo de entrada de dados

dadosEntrada = [line.strip() for line in open(arquivoEntrada)]
if len(dadosEntrada) == 0:
  raise Exception('Dados em branco')
tipoGrafo = str(dadosEntrada.pop(0)).lower()
quantidadeVertices = int(dadosEntrada.pop(0))


# Como não foi especificado se os dados de `input` quando tiverem os mesmos 
# vertice origem e destino, indicando um loop, seria sempre um loop de x->x e x<-x, ficando em uma matriz a soma com 2 em vez de 1
# Caso o valor for 1, seria necessario dois input de entrada por exemplo:
# 2 2
# 2 2
# ou 
# 2-2,2
# para validar que é uma Grafo nao direcionado
# se o valor for 2 é necessario apenas uma entrada para validar como um Grafo nao direcionado
# 2 2
# ou
# 2-2
verticesDiagonalSomaMatriz = 2
verticesDiagonalSomaLista = 2



with open('output.txt', "w+") as f: # Arquivo de saida de dados
  with f as sys.stdout:
    
      #Grafo de lista adjacencia
      if tipoGrafo == "l":
        from GrafoLista import Grafo as Grafo  
        grafo = Grafo(quantidadeVertices,verticesDiagonalSomaLista)
        for linha in dadosEntrada:
      	  dados = linha.split('-')
      	  verticeDe = int(dados[0])
      	  verticesPara = map(int,dados[1].split(','))
      	  for x in verticesPara:
      	  	grafo.AdicionaAresta(verticeDe,x)

      #Grafo de matriz adjacencia
      elif tipoGrafo == "m":
        from GrafoMatriz import Grafo as Grafo
        grafo = Grafo(quantidadeVertices,verticesDiagonalSomaMatriz);
        for linha in dadosEntrada:
      	  aresta = map(int,linha.split(' '))
      	  grafo.AdicionaAresta(aresta[0],aresta[1])

      #Tipo de grafo invalido
      else:
        raise Exception('Tipo de grafo invalido')

      
      print grafo.VerificaDirecionado()  
      print grafo.Grau(1)
      print grafo.GrauEntrada(1)
      print grafo.GrauSaida(1)
      print grafo.Transposto()
      print grafo.VerificaDirecionado()